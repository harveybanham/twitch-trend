app.controller("homeController", ['$scope', '$interval', 'homeFactory', function($scope, $interval, homeFactory) {
    $scope.pageType = "stream-page";
    
    init();
    
    //Sorting stream list
    $scope.sortStreams = function(streamOrderProp, sortOrder) {
        // $scope.reverse = ($scope.streamOrderProp === streamOrderProp) ? !$scope.reverse : false;
        $scope.streamSortOrder = sortOrder;
        $scope.streamOrderProp = streamOrderProp;
    };
    
    //Viewers charts
    $scope.chartVSeries = ['Viewers'];
    $scope.chartVColors = ['#BB342F'];
    
    //Followers charts
    $scope.chartFSeries = ['Followers'];
    $scope.chartFColors = ['#6441A4'];
    
    //Chart options
    $scope.chartOptions = {
        scales: {
            yAxes: [{
                ticks: {
                    maxTicksLimit: 3,
                    userCallback: function(label, index, labels) {
                        //Only return integer values, no decimals
                        if (Math.floor(label) === label) {
                            //Add thousand separators
                            return label.toLocaleString();
                        }
                    }
                }
            }],
            xAxes: [{
                ticks: {
                    maxTicksLimit: 7
                }
            }]
        }
    };
    
    function init() {
        $scope.streamOrderProp = 'currentViewers';
        $scope.streamSortOrder = true;
        
        var updateStreamsInterval = $interval(updateStreams, 600000);
        updateStreams();
        
        //Clear interval between pages/scopes
        $scope.$on("$destroy", function() {
            $interval.cancel(updateStreamsInterval);
        });
    }
    
    function updateStreams() {
        homeFactory.getTwStreams().then(function successCallback(response) {
            //Success, update twitch streams
            $scope.twStreams = response.data;
            
            //Update list of games
            $scope.games = _.keys(_.countBy(response.data, function(data) {
                //Don't add empty game values
                if(data.game != "") { return data.game; }
            }));
            
        }, function errorCallback(response) {
            console.log("Error: " + response);
        });
    }
}]);
