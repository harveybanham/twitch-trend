app.factory('homeFactory', function($http) {
    var factory = {};
    
    factory.getTwStreams = function() {
        var request = {
            method: 'GET',
            url: '/get-twitch-streams'
        };
        
        return $http(request);
    }
    
    return factory;
});


//I tried to move the data more into the factory rather than the controller, it didn't work so well...
// app.factory('homeFactory', ['$http', '$rootScope', '$interval', '$timeout', function($http, $rootScope, $interval, $timeout) {
//     var factory = {};
//     var twData = [];
//     
//     function requestTwData() {
//         var request = {
//             method: 'GET',
//             url: '/get-twitch-streams'
//         };
//         
//         $http(request).then(function successCallback(response) {
//             console.log("HTTP REQUEST");
//             twData = response.data;
//             $rootScope.$broadcast('newData', { data: twData });
//         }, function failureCallback(reason) {
//             console.log(reason);
//         })
//     }
//     
//     factory.getTwStreams = function() {
//         if(twData.length > 0) {
//             console.log("SHOWING OLD DATA");
//             console.log(twData);
//             return twData;
//             // return $rootScope.$broadcast('newData', { data: twData });
//         }
//         else {
//             requestTwData();
//             $interval(function() {
//                 console.log("GETTING NEW DATA");
//                 return requestTwData();
//             }, 15000);
//         }
//     }
//     
//     return factory;
// }]);


//homeController code
// $scope.twStreams = homeFactory.getTwStreams();
// 
// $scope.$on('newData', function(event, args) {
//     console.log("NEW DATA");
//     $scope.twStreams = args.data;
// });
