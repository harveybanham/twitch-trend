var fs = require('fs');
var https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors');
var fs = require('fs');
var moment = require('moment');

var config = {};
var twStreamsFile = "twitch-streams.json";

if (fs.existsSync("./config.json")) {
    config = require("./config.json");
} else {
    config.CLIENT_ID = process.env.CLIENT_ID;
}

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('build'));

//For Heroku
var server_port = process.env.PORT || 3000;

app.listen(server_port, function() {
    console.log('listening on ' + server_port);
});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});
app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});
app.get('/contact', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});

app.get('/get-twitch-streams', function (req, res) {
    res.sendFile(__dirname + "/" + twStreamsFile);
});

//Loop to get data every 10 seconds
//node-schedule may be better for more advanced scheduling
//https://github.com/node-schedule/node-schedule
setInterval(getTwStreams, 600000);
getTwStreams();

function getTwStreams() {
    var output = [];
    
    var request = {
        host: 'api.twitch.tv',
        path: '/kraken/streams/?limit=30&client_id=' + config.CLIENT_ID
    };
    
    https.get(request, function(response) {
        var json = '';
        var timeNow = moment().format('HH:mm');
        response.setEncoding('utf8');
        
        if(response.statusCode === 200) {
            response.on('data', function(chunk) {
                json += chunk;
            }).on('end', function() {
                var twichStreamsObj = JSON.parse(json);
                
                twichStreamsObj.streams.forEach(function(twStream) {
                    var outputObj = {};
                    outputObj.id = twStream._id;
                    outputObj.game = twStream.game;
                    outputObj.currentViewers = twStream.viewers;
                    outputObj.preview = twStream.preview.large;
                    outputObj.name = twStream.channel.display_name;
                    outputObj.currentFollowers = twStream.channel.followers;
                    outputObj.url = twStream.channel.url;
                    output.push(outputObj);
                });
                
                //Loop to merge old data, for an over-time look
                var oldStreamInfo = [];
                try {
                    oldStreamInfo = JSON.parse(fs.readFileSync(twStreamsFile, 'utf8'));
                }
                catch(err) {
                    if(err.code == "ENOENT") {
                        console.log("File not found: " + twStreamsFile);
                    }
                }
                
                //Loop NEW data
                output.forEach(function(newObj) {
                    var found = false;
                    
                    //Loop OLD data
                    oldStreamInfo.forEach(function(oldObj) {
                        if(!found) {
                            //Found old data, merge
                            if(newObj.id == oldObj.id) {
                                newObj.labels = oldObj.labels;
                                newObj.labels.push(timeNow);
                                newObj.viewers = oldObj.viewers;
                                newObj.followers = oldObj.followers;
                                newObj.viewers[0].push(newObj.currentViewers);
                                newObj.followers[0].push(newObj.currentFollowers);
                                found = true;
                            }
                        }
                    });
                    
                    if(!found) {
                        //No old data, create new arrays
                        //Viewers+Followers needs to be an array of arrays for angular-chart
                        newObj.labels = [timeNow];
                        newObj.viewers = [[newObj.currentViewers]];
                        newObj.followers = [[newObj.currentFollowers]];
                    }
                });
                
                fs.writeFileSync(twStreamsFile, JSON.stringify(output));
            });
        } else {
            console.log('Error : ' + reseponse.statusCode);
        }
    }).on('error', function(e) {
        console.log('Error : ' + e.message);
    });
}















